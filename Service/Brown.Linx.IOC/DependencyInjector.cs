using System.Reflection;

using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MySQL.Data.EntityFrameworkCore.Extensions;

using Brown.Linx.Configuration;
using Brown.Linx.Database;
using Brown.Linx.Domain.Configuration;
using Brown.Linx.Domain.Services;
using Brown.Linx.Services;
using Brown.Linx.Domain.Repositories;
using Brown.Linx.Database.Repositories;
using Brown.Linx.Mapper;

namespace Brown.Linx.IOC
{
    public class DependencyInjector
    {
        protected IConfigurationRoot _configuration;

        public void Configure(IServiceCollection services, string rootPath, string environment)
        {
            Configuration(services, rootPath, environment);
            Repositories(services);
            Services(services);
            Extensions(services);
        }
 
        void Configuration(IServiceCollection services, string rootPath, string environment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(rootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true)
                .AddEnvironmentVariables();

            _configuration = builder.Build();

            services.AddSingleton(sp => {
                return builder.Build();
            });

            services.AddSingleton<IDatabaseConfig, DatabaseConfig>();
            services.AddSingleton<IJwtTokenConfig, JwtTokenConfig>();
            services.AddSingleton<INavigationalPropertyCache, NavigationalPropertyCache>();
        }

        void Repositories(IServiceCollection services)
        {
            services.AddScoped<IDbContext, DatabaseContext>(sp => {
                IDatabaseConfig config = sp.GetService<IDatabaseConfig>();
                INavigationalPropertyCache propertyCache = sp.GetService<INavigationalPropertyCache>();

                return new DatabaseContext(config, propertyCache);
            });

            // services.AddScoped<IUserRepository, UserRepository>();
        }

        void Services(IServiceCollection services)
        {
            // services.AddScoped<IUserService, UserService>();
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddSingleton(typeof(IMapping<,>), typeof(Mappings<,>));
            services.AddScoped<ITokenService, JwtTokenService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
        }

        void Extensions(IServiceCollection services)
        {
            services.AddRouting();
            services.AddMvc();

            services.AddDbContext<DatabaseContext>(options =>
            {
                options.UseMySQL(_configuration.GetConnectionString("Brown.LinxContext"));
            });
 
            services.AddAutoMapper(new[] {
                Assembly.Load(new AssemblyName("Brown.Linx.Mapper"))
            });
        }
    }
}