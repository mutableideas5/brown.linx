using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;

using Brown.Linx.Domain.Configuration;
using Brown.Linx.Domain.Models;
using Brown.Linx.Domain.Services;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Linq;

namespace Brown.Linx.Services
{
    public class JwtTokenService : ITokenService
    {
        readonly IJwtTokenConfig _jwtTokenConfig;

        SymmetricSecurityKey _securityKey;
        JwtSecurityTokenHandler _tokenHandler;

        JwtSecurityTokenHandler TokenHandler
        {
            get {
                _tokenHandler = _tokenHandler ?? new JwtSecurityTokenHandler();
                return _tokenHandler;
            }
        }

        SymmetricSecurityKey SecurityKey
        {
            get {
                _securityKey = _securityKey ?? GetSecurityKey();
                return _securityKey;
            }
        }

        public JwtTokenService(IJwtTokenConfig jwtTokenConfig)
        {
            _jwtTokenConfig = jwtTokenConfig;
        }

        public string GenerateToken(IEnumerable<Claim> claims)
        {
            ClaimsIdentity identity = new ClaimsIdentity();
            identity.AddClaims(claims);
 
            return TokenHandler.CreateEncodedJwt(_jwtTokenConfig.Issuer,
                _jwtTokenConfig.Audience,
                identity,
                DateTime.UtcNow.AddMinutes(-1),
                DateTime.UtcNow.AddMinutes(_jwtTokenConfig.ExpirationMinutes),
                DateTime.UtcNow,
                new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256Signature));
        }

        public ClaimsPrincipal GetClaims(string token)
        {
            if (!IsValidToken(token))
            {
                throw new UnauthorizedAccessException("Invalid Token");
            }

            return GetClaimsPrincipal(token);
        }

        public bool IsValidToken(string token)
        {
            return GetClaimsPrincipal(token) != null;
        }
 
        ClaimsPrincipal GetClaimsPrincipal(string token)
        {
            SecurityToken securityToken;
            TokenValidationParameters parameters = new TokenValidationParameters
            {
                IssuerSigningKey = SecurityKey,
                ValidateIssuerSigningKey = true,

                ValidIssuer = _jwtTokenConfig.Issuer,
                ValidateIssuer = true,

                ValidAudience = _jwtTokenConfig.Audience,
                ValidateAudience = true,

                ValidateLifetime = true
            };

            ClaimsPrincipal claimsPrincipal = TokenHandler.ValidateToken(token, parameters, out securityToken);
            return claimsPrincipal;
        }

        SymmetricSecurityKey GetSecurityKey()
        {
            string executingDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            byte[] key = File.ReadAllBytes($"{executingDirectory}/{_jwtTokenConfig.KeyPath}");
            return new SymmetricSecurityKey(key);
        }
    }
}