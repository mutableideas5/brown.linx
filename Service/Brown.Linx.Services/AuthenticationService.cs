using System;
using System.Collections.Generic;
using System.Linq;

using Brown.Linx.Domain.Models;
using Brown.Linx.Domain.Services;
using System.Security.Claims;

namespace Brown.Linx.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        readonly ITokenService _tokenService;

        public AuthenticationService(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        public TokenModel AuthenticateUser(LoginModel model)
        {
            // user service allowing user to login to the system
            return GetToken(new UserModel());
        }

        public TokenModel RefreshToken(ClaimsPrincipal principal)
        {
            // string userId = principal.Claims.First(p => p.Type == UserClaimTypes.USERID).Value;
            return GetToken(new UserModel());
        }

        public TokenModel GetToken(UserModel userModel)
        {
            if (userModel == null)
                throw new UnauthorizedAccessException("Invalid username or password.");

            IEnumerable<Claim> claims = new List<Claim>();
            return new TokenModel {
                Token = _tokenService.GenerateToken(claims)
            };
        }
    }
}