using System;
using System.Collections.Generic;
using System.Linq;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;
using Brown.Linx.Domain.Repositories;
using Brown.Linx.Domain.Services;

namespace Brown.Linx.Services
{
    public  class CrudService<T, TModel> : ReadonlyService<T, TModel>, ICrudService<T, TModel>
        where T : Entity
        where TModel : EntityModel
    {

        readonly ICrudRepository<T> _crudRepository;
        readonly IMapping<T, TModel> _mapping;

        public CrudService(ICrudRepository<T> crudRepository, IMapping<T, TModel> mapping) : base(crudRepository, mapping)
        {
            _crudRepository = crudRepository;
            _mapping = mapping;
        }

        public virtual TModel Add(TModel model)
        {
            T entity = _mapping.Map(model);

            entity.Id = Guid.NewGuid().ToString();
            _crudRepository.Insert(entity);

            return _mapping.Map(entity);
        }

        public IEnumerable<TModel> AddMany(IEnumerable<TModel> models)
        {
            IEnumerable<T> entities = _mapping.Map(models).Select(entity => {
                entity.Id = Guid.NewGuid().ToString();
                return entity;
            });

            entities = _crudRepository.InsertMany(entities);
            return entities.Select(entity => _mapping.Map(entity));
        }

        public virtual void UpdateMany(IEnumerable<TModel> models)
        {
            IEnumerable<T> entities = _mapping.Map(models);
            _crudRepository.UpdateMany(entities);
        }

        public virtual void Delete(string id)
        {
            _crudRepository.Remove(id);
        }

        public virtual void Update(TModel model)
        {
            T entity = _crudRepository.GetById(model.Id);
            entity = _mapping.Map(model, entity);
            _crudRepository.Update(entity);
        }
    }
}