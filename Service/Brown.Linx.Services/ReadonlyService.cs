using System;
using System.Collections.Generic;
using System.Linq;

using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;
using Brown.Linx.Domain.Repositories;
using Brown.Linx.Domain.Services;

namespace Brown.Linx.Services
{
    public abstract class ReadonlyService<T, TModel> : IReadonlyService<T, TModel>
        where T : Entity
        where TModel : EntityModel
    {
        readonly IReadonlyRepository<T> _readonlyRepository;
        readonly IMapping<T, TModel> _mapping;

        public ReadonlyService(IReadonlyRepository<T> readonlyRepository, IMapping<T, TModel> mapping)
        {
            _readonlyRepository = readonlyRepository;
            _mapping = mapping;
        }

        public virtual TModel GetById(string id)
        {
            T entity = _readonlyRepository.GetById(id);
            return _mapping.Map(entity);
        }

        public virtual IEnumerable<TModel> LoadAll()
        {
            IEnumerable<T> entities = _readonlyRepository.LoadAll();
            return entities.Select(_mapping.Map);
        }
    }
}
