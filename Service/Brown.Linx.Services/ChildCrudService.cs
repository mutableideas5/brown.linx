using System.Collections.Generic;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;
using Brown.Linx.Domain.Repositories;
using Brown.Linx.Domain.Services;

namespace Brown.Linx.Services
{
    public abstract class ChildCrudService<T, TModel, P> : CrudService<T, TModel>, IChildCrudService<T, TModel>
        where T : Entity
        where TModel : EntityModel
    {
        readonly IChildCrudRepository<T> _crudRepository;
        readonly IMapping<T, TModel> _mapping;

        public ChildCrudService(IChildCrudRepository<T> crudRepository, IMapping<T, TModel> mapping) : base(crudRepository, mapping)
        { 
            _crudRepository = crudRepository;
            _mapping = mapping;
        }

        public IEnumerable<TModel> GetByParentId(string parentId)
        {
            IEnumerable<T> entities = _crudRepository.GetByParentId(parentId);
            return _mapping.Map(entities);
        }
    }
}