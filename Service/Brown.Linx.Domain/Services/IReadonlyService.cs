using System;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;
using System.Collections.Generic;

namespace Brown.Linx.Domain.Services
{
    public interface IReadonlyService<T, TModel>
        where T : Entity
        where TModel : EntityModel
    {
        TModel GetById(string id);
        IEnumerable<TModel> LoadAll();
    }
}
