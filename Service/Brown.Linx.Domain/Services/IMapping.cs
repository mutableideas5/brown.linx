using System.Collections.Generic;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;

namespace Brown.Linx.Domain.Services
{
    public interface IMapping<T, TModel>
        where T : Entity
        where TModel : EntityModel
    {
        T Map(TModel model);
        TModel Map(T entity);

        TModel Map(T entity, TModel model);
        T Map(TModel model, T entity);

        IEnumerable<T> Map(IEnumerable<TModel> models);
        IEnumerable<TModel> Map(IEnumerable<T> entities);
    }
}