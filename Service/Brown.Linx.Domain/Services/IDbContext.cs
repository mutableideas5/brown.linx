using System;
using System.Collections.Generic;
using System.Linq;
using Brown.Linx.Domain.Entities;

namespace Brown.Linx.Domain.Services
{
    public interface IDbContext : INavigationalPropertyCache, IDisposable
    {
        IQueryable<T> GetTable<T>() where T : Entity;

        T Insert<T>(T entity) where T : Entity;
        IEnumerable<T> InsertMany<T>(IEnumerable<T> entities) where T : Entity;
        IEnumerable<T> ReviseMany<T>(IEnumerable<T> entities) where T : Entity;

        T Revise<T>(T entity) where T : Entity;

        void Delete<T>(T entity) where T : Entity;

        int SaveChanges();
    }
}
