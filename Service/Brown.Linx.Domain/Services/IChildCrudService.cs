using System.Collections.Generic;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;

namespace Brown.Linx.Domain.Services
{
    public interface IChildCrudService<T, TModel> : ICrudService<T, TModel>
        where T : Entity
        where TModel : EntityModel
    {
        IEnumerable<TModel> GetByParentId(string parentId); 
    }
}