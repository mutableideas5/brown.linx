using System;
using System.Collections.Generic;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;

namespace Brown.Linx.Domain.Services
{
    public interface ICrudService<T, TModel> : IReadonlyService<T, TModel>
        where T : Entity
        where TModel : EntityModel
    {
        TModel Add(TModel model);
        IEnumerable<TModel> AddMany(IEnumerable<TModel> models);
        void UpdateMany(IEnumerable<TModel> models);
        void Update(TModel model);
        void Delete(string id);
    }
}