using System;
using System.Collections.Generic;
using System.Security.Claims;
using Brown.Linx.Domain.Models;

namespace Brown.Linx.Domain.Services
{
    public interface ITokenService
    {
        string GenerateToken(IEnumerable<Claim> claims);
        ClaimsPrincipal GetClaims(string token);
        bool IsValidToken(string token);
    }
}