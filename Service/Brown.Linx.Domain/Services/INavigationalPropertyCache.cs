using System;
using System.Collections.Generic;

namespace Brown.Linx.Domain.Services
{
    public interface INavigationalPropertyCache
    {
        IDictionary<Type, IEnumerable<string>> NavigationProperties { get; }
    }
}