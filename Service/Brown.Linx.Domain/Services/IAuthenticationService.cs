using System.Collections.Generic;
using System.Security.Claims;
using Brown.Linx.Domain.Models;

namespace Brown.Linx.Domain.Services
{
    public interface IAuthenticationService
    {
        TokenModel AuthenticateUser(LoginModel model);
        TokenModel RefreshToken(ClaimsPrincipal claimsPrincipal);
        TokenModel GetToken(UserModel userModel);
    }
}