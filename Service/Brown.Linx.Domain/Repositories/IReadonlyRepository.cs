using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using Brown.Linx.Domain.Entities;

namespace Brown.Linx.Domain.Repositories
{
    public interface IReadonlyRepository<T> where T : Entity
    {
        T GetById(string id);
        IEnumerable<T> LoadAll();
        IEnumerable<T> Where(Expression<Func<T, bool>> predicate);
    }
}