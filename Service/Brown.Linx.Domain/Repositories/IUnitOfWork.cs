using System;

namespace Brown.Linx.Domain.Repositories
{
    public interface IUnitOfWork
    {
        void Work(Action action);
    }
}