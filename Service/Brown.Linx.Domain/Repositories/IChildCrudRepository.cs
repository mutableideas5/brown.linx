using System.Collections.Generic;
using Brown.Linx.Domain.Entities;

namespace Brown.Linx.Domain.Repositories
{
    public interface IChildCrudRepository<T> : ICrudRepository<T>
        where T : Entity
    {
        IEnumerable<T> GetByParentId(string parentId);
    }
}