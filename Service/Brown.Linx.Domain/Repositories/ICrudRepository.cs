using System.Collections.Generic;
using Brown.Linx.Domain.Entities;

namespace Brown.Linx.Domain.Repositories
{
    public interface ICrudRepository<T> : IReadonlyRepository<T> where T : Entity
    {
        T Insert(T entity);
        T Update(T entity);
        IEnumerable<T> UpdateMany(IEnumerable<T> entities);
        IEnumerable<T> InsertMany(IEnumerable<T> entities);
    
        void Remove(T entity);
        void Remove(string id);
    }
}