namespace Brown.Linx.Domain.Models
{
    public class TokenModel
    {
        public string Token { get; set; }
    }
}