namespace Brown.Linx.Domain.Models
{
    public abstract class EntityModel
    {
        public string Id { get; set; }
    }
}