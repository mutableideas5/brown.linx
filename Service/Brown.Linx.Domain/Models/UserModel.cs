namespace Brown.Linx.Domain.Models
{
    public class UserModel : EntityModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
    }
}