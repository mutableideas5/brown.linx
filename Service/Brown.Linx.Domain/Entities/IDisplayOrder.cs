namespace Brown.Linx.Domain.Entities
{
    public interface IDisplayOrder
    {
        int DisplayOrder { get; set;}
    }
}