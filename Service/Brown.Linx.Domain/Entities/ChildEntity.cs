namespace Brown.Linx.Domain.Entities 
{
    public abstract class ChildEntity : Entity
    {
        public abstract string ParentId { get; set; }
    }
}