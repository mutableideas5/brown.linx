using System;
using System.Collections.Generic;
using System.Text;

namespace Brown.Linx.Domain.Entities
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}