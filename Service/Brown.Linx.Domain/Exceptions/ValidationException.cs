using System;

namespace Brown.Linx.Domain.Exceptions
{
    public class ValidationException : Exception
    {
        public string FieldName { get; private set; }

        public ValidationException(string fieldName, string mesasge) : base(mesasge)
        {
            FieldName = fieldName;
        } 
    }
}