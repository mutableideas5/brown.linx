namespace Brown.Linx.Domain.Configuration
{
    public interface IDatabaseConfig
    {
        string ConnectionString { get; }
    }
}