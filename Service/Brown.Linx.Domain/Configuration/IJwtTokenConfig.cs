using System;
using System.Collections.Generic;
using System.Text;

namespace Brown.Linx.Domain.Configuration
{
    public interface IJwtTokenConfig
    {
        string Issuer { get; }
        string Audience { get; }
        string KeyPath { get; }
        int ExpirationMinutes { get; }
    }
}
