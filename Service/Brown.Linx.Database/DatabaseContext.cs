using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MySQL.Data.EntityFrameworkCore.Extensions;

using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Services;
using Brown.Linx.Domain.Configuration;
using System;

namespace Brown.Linx.Database
{
    public class DatabaseContext : DbContext, IDbContext
    {
        readonly Dictionary<string, object> _tables = new Dictionary<string, object>();
        readonly IDatabaseConfig _databaseConfig;
        public IDictionary<Type, IEnumerable<string>> NavigationProperties { get; }

        /* public DbSet<User> Users { get; set; } */

        public DatabaseContext(DbContextOptions options) : base(options) { }

        public DatabaseContext() : base() { }

        public DatabaseContext(IDatabaseConfig databaseConfig, INavigationalPropertyCache navigationalPropertyCache)
        {
            NavigationProperties = navigationalPropertyCache.NavigationProperties;
            _databaseConfig = databaseConfig;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            /*builder.Entity<User>(usr => {
                usr.HasKey(p => p.Id);
                usr.Property(p => p.Id).HasMaxLength(36).IsRequired();

                usr.Property(p => p.Name).HasMaxLength(100).IsRequired();
                usr.Property(p => p.Email).HasMaxLength(100).IsRequired();
                usr.Property(p => p.Password).HasMaxLength(256).IsRequired();
                usr.Property(p => p.PromotionList).HasDefaultValue(true);

                usr.HasIndex(p => p.Email)
                    .HasName("UK_Users_Email")
                    .IsUnique();
            });*/
        }       

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            // check here due to migrations
            if (_databaseConfig != null)
            {
                builder.UseMySQL(_databaseConfig.ConnectionString);
                // builder.UseSqlServer(_databaseConfig.ConnectionString);
            }

            base.OnConfiguring(builder);
        }

        public void Delete<T>(T entity) where T : Entity
        {
            Remove(entity);
        }

        public IQueryable<T> GetTable<T>() where T : Entity
        {
            string typeName = typeof(T).FullName.ToLower();
            DbSet<T> table;

            if (!_tables.ContainsKey(typeName))
            {
                table = Set<T>();
                _tables.Add(typeName, table);
            }
            else
            {
                table = _tables[typeName] as DbSet<T>;
            }

            return table.AsQueryable();
        }

        public T Insert<T>(T entity) where T : Entity
        {
            return Add(entity).Entity;
        }

        public T Revise<T>(T entity) where T : Entity
        {
            return Update(entity).Entity;
        }

        public IEnumerable<T> InsertMany<T>(IEnumerable<T> entities) where T : Entity
        {
            return entities.Select(entity => Add<T>(entity).Entity).ToArray();
        }

        public IEnumerable<T> ReviseMany<T>(IEnumerable<T> entities) where T : Entity
        {
            return entities.Select(entity => Update<T>(entity).Entity).ToArray();
        }
  }
}