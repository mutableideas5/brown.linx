using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Reflection;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Services;

namespace Brown.Linx.Database
{
  public class NavigationalPropertyCache : INavigationalPropertyCache
  {
    public IDictionary<Type, IEnumerable<string>> NavigationProperties { get; }

    public NavigationalPropertyCache()
    {
      NavigationProperties = new Dictionary<Type, IEnumerable<string>>();
      Configure();
    }

    void Configure()
    {
      IEnumerable<Type> entityTypes = Assembly.Load(new AssemblyName("Brown.Linx.Domain")).GetTypes()
        .Where(typ => typ.GetTypeInfo().IsSubclassOf(typeof(Entity)) && !typ.GetTypeInfo().IsAbstract).ToArray();
      
      foreach(Type entityType in entityTypes)
      {
        IEnumerable<string> propertyNames = GetNavigationalProperties(entityType);
        NavigationProperties.Add(entityType, propertyNames);
      }
    }

    static IEnumerable<string> GetNavigationalProperties(Type entityType)
    {
      // Type entityType = typeof(Entity);

      return entityType.GetProperties()
        .Where(p => (typeof(IEnumerable).IsAssignableFrom(p.PropertyType) && p.PropertyType != typeof(string)) ||  p.PropertyType.Namespace == entityType.Namespace)
        .Select(p => p.Name)
        .ToArray();
      /*PropertyInfo[] properties = itemType.GetProperties()
          .Where(p =>
            p.PropertyType.GetTypeInfo().IsGenericType
            && p.PropertyType.GetTypeInfo().GenericTypeArguments[0].GetTypeInfo().IsSubclassOf(entityType))
          .ToArray();

      return properties.Select(p => p.Name);*/
    }
  }
}