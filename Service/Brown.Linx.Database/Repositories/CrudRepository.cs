using System;
using System.Collections.Generic;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Repositories;
using Brown.Linx.Domain.Services;

namespace Brown.Linx.Database.Repositories
{
    public abstract class CrudRepository<T> : ReadonlyRepository<T>, ICrudRepository<T>
        where T : Entity
    {
        readonly IDbContext _dbContext;

        public CrudRepository(IDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public T Insert(T entity)
        {
            return _dbContext.Insert(entity);
        }

        public IEnumerable<T> InsertMany(IEnumerable<T> entities)
        {
            return _dbContext.InsertMany(entities);
        }

        public void Remove(T entity)
        {
            _dbContext.Delete(entity);
        }

        public void Remove(string id)
        {
            T entity = GetById(id);
            Remove(entity);
        }

        public T Update(T entity)
        {
            return _dbContext.Revise(entity);
        }

        public IEnumerable<T> UpdateMany(IEnumerable<T> entities)
        {
            return _dbContext.ReviseMany(entities);
        }
  }
}