using System;
using System.Collections.Generic;

using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Repositories;
using Brown.Linx.Domain.Services;
using System.Linq;
using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Brown.Linx.Database.Repositories
{
    public abstract class ReadonlyRepository<T> : IReadonlyRepository<T>
        where T : Entity
    {
        readonly IDbContext _context;

        public ReadonlyRepository(IDbContext context)
        {
            _context = context;
        }

        public T GetById(string id)
        {
            return Where(p => p.Id.Equals(id)).FirstOrDefault();
        }

        public IEnumerable<T> LoadAll()
        {
            return LoadEagerly();
        }

        public IEnumerable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return LoadEagerly().Where(predicate).AsEnumerable();
        }

        IQueryable<T> LoadEagerly()
        {
            Type itemType = typeof(T);
            IEnumerable<string> properties = _context.NavigationProperties[itemType];
            IQueryable<T> table = _context.GetTable<T>();

            foreach(string propName in properties)
            {
                table = table.Include(propName);
            }

            return table;
        }
    }
}