using System;
using System.Collections.Generic;
using Brown.Linx.Domain.Repositories;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Services;

namespace Brown.Linx.Database.Repositories
{
	public abstract class ChildCrudRepository<T> : CrudRepository<T>, IChildCrudRepository<T>
    	where T : Entity
  	{
    	public ChildCrudRepository(IDbContext dbContext) : base(dbContext) { }

    	public abstract IEnumerable<T> GetByParentId(string parentId);
  	}
}