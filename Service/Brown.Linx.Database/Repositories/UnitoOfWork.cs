using System;

using Brown.Linx.Domain.Repositories;
using Brown.Linx.Domain.Services;

namespace Brown.Linx.Database.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly IDbContext _dbContext;

        public UnitOfWork(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Work(Action action)
        {
            action();
            Commit();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            GC.SuppressFinalize(this);
        }

        protected virtual int Commit()
        {
            return _dbContext.SaveChanges();
        }
    }
}
