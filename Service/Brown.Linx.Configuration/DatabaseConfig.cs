using System;
using Brown.Linx.Domain.Configuration;
using Microsoft.Extensions.Configuration;

namespace Brown.Linx.Configuration
{
    public class DatabaseConfig : IDatabaseConfig
    {
        readonly IConfigurationRoot _configRoot;

        public DatabaseConfig(IConfigurationRoot configRoot)
        {
            _configRoot = configRoot;
        }

        public string ConnectionString {
            get
            {
                return _configRoot.GetConnectionString("Brown.LinxContext");
            }
        }
    }
}