using System;
using Brown.Linx.Domain.Configuration;
using Microsoft.Extensions.Configuration;

namespace Brown.Linx.Configuration
{
    public class JwtTokenConfig : IJwtTokenConfig
    {
        IConfigurationSection _configSection;

        public JwtTokenConfig(IConfigurationRoot configRoot) => _configSection = configRoot.GetSection("token");

        public string Issuer => _configSection["issuer"];

        public string Audience => _configSection["audience"];

        public string KeyPath => _configSection["keyPath"];

        public int ExpirationMinutes => int.Parse(_configSection["expirationMinutes"]);

    }
}
