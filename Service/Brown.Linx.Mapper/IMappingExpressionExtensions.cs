using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;

namespace Brown.Linx.Mapper
{
    public static class IMappingExpressionExtensions
    {
        public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> map,
            Expression<Func<TDestination, object>> selector)
        {
            map.ForMember(selector, config => config.Ignore());
            return map;
        }

        public static IMappingExpression<TSource, TDestination> Map<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> map,
            Expression<Func<TDestination, object>> selector,
            Expression<Func<TSource, object>> srcSelector)
        {
            map.ForMember(selector, config => config.MapFrom(srcSelector));
            return map;
        }
    }
}