using Brown.Linx.Domain.Entities;
using Brown.Linx.Domain.Models;
using Brown.Linx.Domain.Services;
using AutoMapper;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Brown.Linx.Mapper
{
    public class Mappings<T, TModel> : IMapping<T, TModel>
        where T : Entity
        where TModel : EntityModel
    {

        readonly IMapper _mapper;

        public Mappings(IMapper mapper)
        {
            _mapper = mapper;
        }

        public T Map(TModel model)
        {
            return _mapper.Map<TModel, T>(model);
        }

        public TModel Map(T entity)
        {
            return _mapper.Map<T, TModel>(entity);
        }

        public T Map(TModel model, T entity)
        {
            return _mapper.Map(model, entity);
        }

        public TModel Map(T entity, TModel model)
        {
            return _mapper.Map(entity, model);
        }

        public IEnumerable<T> Map(IEnumerable<TModel> models)
        {
            return models.Select(model => Map(model));
        }

        public IEnumerable<TModel> Map(IEnumerable<T> entities)
        {
            return entities.Select(entity => Map(entity));
        }
    }
}