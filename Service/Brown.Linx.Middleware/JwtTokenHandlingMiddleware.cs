using System.Linq;
using System.Threading.Tasks;
using Brown.Linx.Domain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System.Security.Claims;

namespace Brown.Linx.Middleware {
    public class JwtTokenHandlingMiddleWare {
        readonly RequestDelegate _next;

        public JwtTokenHandlingMiddleWare(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            const string authKey = "Authorization";
            StringValues headerValues;
            IHeaderDictionary headers = context.Request.Headers;

            if (headers.TryGetValue(authKey, out headerValues) && headerValues.Count > 0) {
                ITokenService tokenService = context.RequestServices.GetService(typeof(ITokenService)) as ITokenService;
                string token = headerValues.First();

                context.User = tokenService.GetClaims(token);
            }

            await _next(context);
        }
    }
}