using System;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Brown.Linx.Domain.Exceptions;

namespace Brown.Linx.Middleware
{
    public class ErrorHandlingMiddleware
    {
        readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            int statusCode = (int)HttpStatusCode.InternalServerError;
            string message = "An unexpected error occurred while prcessing your request.";

            if (ex is ValidationException)
            {
                statusCode = (int)HttpStatusCode.BadRequest;
                message = ex.Message;
            }

            if (ex is UnauthorizedAccessException)
            {
                statusCode = (int)HttpStatusCode.Forbidden;
                message = "The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible.";
            }

            if (ex is SecurityTokenValidationException)
            {
                statusCode = (int)HttpStatusCode.Unauthorized;
                message = "Invalid access token.";
            }

            // need to create specific status codes
            context.Response.StatusCode = statusCode;
            context.Response.ContentType = "application/json";
 
            return context.Response.WriteAsync(JsonConvert.SerializeObject(message));
        }
    }
}